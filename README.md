# Actibase

This is the production implementation that combines both the server, providing API endpoints via Django Rest Framework; as well as a Node/Express client serving a bundled VueJS interface.