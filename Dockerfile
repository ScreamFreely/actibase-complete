FROM ubuntu as actibase

MAINTAINER Culture Clap <canin@riseup.org>

ENV TZ=America/Chicago
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apt-get update && apt-get install -y \
    python2.7 \
    python-pip \
    python-lxml \
    libssl-dev \
    mdbtools \
    python-dev \
    python3-dev \
    poppler-utils \
    python-virtualenv \
    python3.5 \
    git \
    libpq-dev \
    libgeos-dev \
    libgdal-dev \
    gdal-bin \
    s3cmd \
    freetds-dev \
    curl \
    wget \
    unzip \
    postgresql \
    postgresql-contrib \ 
    gnupg \ 
	dirmngr



USER root


RUN mkdir /opt/homebase
COPY actibase /opt/homebase/
COPY corefiles/requirements.txt /opt/homebase/
RUN virtualenv -p $(which python3) /opt/homebase/venv-actibase

RUN /opt/homebase/venv-actibase/bin/pip install -r /opt/homebase/requirements.txt
COPY corefiles/pupa.settings /opt/homebase/venv-actibase/src/pupa/pupa/settings.py

COPY corefiles/siteauth.py /opt/homebase/

# ENTRYPOINT ["service", "postgresql", "start"]

# RUN /opt/homebase/venv-actibase/bin/pupa dbinit US
# RUN /opt/homebase/venv-actibase/bin/python /opt/homebase/actibase/manage.py migrate

