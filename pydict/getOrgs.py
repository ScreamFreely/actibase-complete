import csv

import sys
import os
import django
from pprint import pprint as ppr


sys.path.append('/var/www/mn.actibase/actibase/')
os.environ['DJANGO_SETTINGS_MODULE'] = 'Actibase.settings'
django.setup()

import re, os
from datetime import datetime

from time import sleep

from opencivicdata.core.models import Jurisdiction, Organization, OrganizationLink


doc = csv.DictReader(open('sortedList.csv', 'r'), delimiter="\t")

MPLS = Jurisdiction.objects.filter(name='Minneapolis')
STP = Jurisdiction.objects.filter(name='Saint Paul')

mpls = []
stp = []

data = []

for d in doc:
	if d['city'] == 'Minneapolis':
		data.append(d)
	elif d['city'] == 'Saint Paul':
		data.append(d)



for d in data:
	name = d['name']
	twit = d['twitter']
	fb = d['fb']
	link = d['link']
	yt = d['yt']
	ig = d['ig']
	if d['city'] === 'Minneapolis':
		city = MPLS
	if d['city'] === 'Saint Paul':
		city = STP

	org, c = Organization.objects.get_or_create(name=name, jurisdiction=city, classification='corporation')
	ppr(org)

	if twit:
		orgLink, c = OrganizationLink.objects.get_or_create(organization=org, url=twit, note='Twitter')
	if fb:
		orgLink, c = OrganizationLink.objects.get_or_create(organization=org, url=fb, note='Facebook')
	if yt:
		orgLink, c = OrganizationLink.objects.get_or_create(organization=org, url=yt, note='YouTube')
	if ig:
		orgLink, c = OrganizationLink.objects.get_or_create(organization=org, url=ig, note='Instagram')
	if link:
		orgLink, c = OrganizationLink.objects.get_or_create(organization=org, url=link, note='Website')